public class Board{
	private Die dice1;
	private Die dice2;
	private boolean[] tiles;
	
	//constructor
	public Board(){
		this.dice1 = new Die();
		this.dice2 = new Die();
		this.tiles = new boolean[12];
	}
	//toString method checking the values of each index in the array
	public String toString(){
		String str = "";
		for(int i = 0; i < this.tiles.length; i++){
			if(tiles[i] == false){
				str += i;
			}
			else{
				str = "X";
			}
		}
		return str;
	}
	
	//method playATurn: playing a turn for the player
	public boolean playATurn(){
		boolean isTileDown = false;
		System.out.println("Dice 1 rolled : " + this.dice1.roll());
		System.out.println("Dice 2 rolled : " + this.dice2.roll());
		
		int sumOfDice = this.dice1.getFaceValue() + this.dice2.getFaceValue();
		
		if(this.tiles[sumOfDice - 1] == false){
			this.tiles[sumOfDice - 1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			isTileDown = false;
		}
		//if the previous tile is already shut, it will check the values of the dice1
		else if(this.tiles[this.dice1.getFaceValue()] == false){
			this.tiles[this.dice1.getFaceValue()] = true;
			System.out.println("Closing tile with the same value as die one: " + this.dice1.getFaceValue());
			isTileDown = false;
		}
		//if the previous tile is already shut, it will check the values of the dice2
		else if(this.tiles[this.dice2.getFaceValue()] == false){
			this.tiles[this.dice2.getFaceValue()] = true;
			System.out.println("Closing tile with the same value as die one: " + this.dice2.getFaceValue());
			isTileDown = false;
		}
		//if all the tiles are already shut
		else{
			System.out.println("All the tiles for these values are already shut");
			isTileDown = true;
		}
		return isTileDown;
	}
}
	