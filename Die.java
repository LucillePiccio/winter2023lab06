import java.util.Random;
public class Die{ 
	private int faceValue;
	private Random rollDice;
	
	//constructor
	public Die(){
		this.faceValue = 1;
		this.rollDice = new Random();
	}
	//getter method for faceValue
	public int getFaceValue(){
		return this.faceValue;
	}
	public int roll(){
		return this.faceValue = this.rollDice.nextInt(6) + 1;
	}
	public String toString(){
		return "Dice rolled " + this.faceValue;
	}
}
	
	