public class Jackpot{
	public static void main (String[]args){
		System.out.println("Welcome to the 2023 Jackpot game!");
		
		//creating board object
		Board b = new Board();
		
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		//loop that repeats until gameOver is false
		while(gameOver == false){
			System.out.println("This is what the tiles look like now " + b.toString());
			if(b.playATurn() == true){
				gameOver = true;
			}
			//when playATurn is false
			else{
				System.out.println("Number of tiles closed is now " + numOfTilesClosed++);
			}
		}
		//checking if numOfTilesClosed => 7
		if(numOfTilesClosed >= 7){
			System.out.println("Congratulations! You have reached the Jackpot!!");
		}
		else{
			System.out.println("Oh no...you lost!");
		}
	}
}